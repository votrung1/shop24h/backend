package com.shop24h.security.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop24h.model.Product;
import com.shop24h.repository.ProductRepository;

@Service
public class ProductService {
    
    @Autowired
    ProductRepository productRepository;


    public List<Product> findProductByKeyName(String name){
        String [] nameArray = name.split(" ");
        List<Product> allProduct = productRepository.findAll();
    
        List<Product> filterProductName = allProduct.stream().filter(product -> {
            for (String keyword : nameArray) {
                if (!product.getProductName().toLowerCase().contains(keyword.toLowerCase())) {
                    return false;
                }
            }
            return true;})
            .collect(Collectors.toList());
        return filterProductName;
    }

    //TÌm kiếm sản phẩm theo tên sản phẩm có phân trang
    // public Page<Product> findProductByKeyNameAndPage(String name, Pageable pageable) {
    //     String[] nameArray = name.split(" ");
    //     Stream<Product> productStream = StreamSupport.stream(productRepository.findAll(pageable).spliterator(), false);
    //     List<Product> productList = productStream.collect(Collectors.toList());
    //     productList = productList.stream()
    //             .filter(product -> {
    //                 for (String keyword : nameArray) {
    //                     if (!product.getProductName().toLowerCase().contains(keyword.toLowerCase())) {
    //                         return false;
    //                     }
    //                 }
    //                 return true;
    //             })
    //             .collect(Collectors.toList());
    
    //     return new PageImpl<>(productList, pageable, productList.size());
    // }        
}
